import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";
import React from "react";
function App() {
  return (
    <div>
      <SwaggerUI url={"taskRest.yaml"} />
    </div>
  );
}

export default App;
